<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <title>Document</title>
</head>
<body>
    <div class="center">
        <div>
            <section class="cert">
                <div>
                    <div class="title">Congratulations!</div>
                    <div class="content">
                        <?php   
                            echo "<span> Điểm bạn đạt được là: ". $_COOKIE['score']."</span>";
                            echo "<br>";
                            if ($_COOKIE['score'] < 4) {
                                echo "<span>Bạn quá kém, cần ôn tập thêm</span>";
                            }
                            else if ($_COOKIE['score'] <= 7) {
                                echo "<span>Cũng bình thường</span>";
                            }
                            else {
                                echo "<span>Sắp sửa làm được trợ giảng lớp PHP</span>";
                            }
                        ?>
                    </div>
                </div>
                <div class="medal"></div>
                <div class="ribbon1"></div>
                <div class="ribbon2"></div>
            </section>
        </div>
    </div>



    
</body>
</html>