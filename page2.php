
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <!-- <link rel="stylesheet" href="./fontawesome-free-6.2.0-web/css/all.css">
    <link rel="stylesheet" href="./fontawesome-free-6.2.0-web/webfonts/fa-brands-400.ttf"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/webfonts/fa-brands-400.ttf" />
    <title>Multiple-choice</title>
</head>
<body>

<?php
$question = array(
    array(
        'question' => '6. "Truyen Kieu" is a famous poem of?',
        'answer' => array(
            'A' => 'Viet Nam',
            'B' => 'China',
            'C' => 'Japan',
            'D' => 'Korea'
        ),
        'correct' => 'A'
    ),
    array(
        'question' => '7. Director of "The Godfather" is?',
        'answer' => array(
            'A' => 'Steven Spielberg',
            'B' => 'Francis Ford Coppola',
            'C' => 'Martin Scorsese',
            'D' => 'Quentin Tarantino'
        ),
        'correct' => 'B'
    ),
    array(
        'question' => '8. The World Cup 2018 is held in?',
        'answer' => array(
            'A' => 'Russia',
            'B' => 'Germany',
            'C' => 'Brazil',
            'D' => 'France'
        ),
        'correct' => 'A'
    ),
    array(
        'question' => '9. League of Legends is a game of?',
        'answer' => array(
            'A' => 'Electronic Arts',
            'B' => 'Blizzard Entertainment',
            'C' => 'Valve Corporation',
            'D' => 'Riot Games'
        ),
        'correct' => 'D'
    ),
    array(
        'question' => '9. Gojo Satoru is a character of?',
        'answer' => array(
            'A' => 'One Piece',
            'B' => 'Naruto',
            'C' => 'Sword Art Online',
            'D' => 'Jujutsu Kaisen'
        ),
        'correct' => 'D'
    ),
);
// save choice by COOKIE

    $correct_choice = array();
    foreach ($question as $key => $value) {
        $correct_choice[$key] = $value['correct'];
    }
    

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['submit'])) {
            echo 'submit';
            $score = $_COOKIE['score'];
            $i = 1;
            $selected_choice = array();
            foreach ($question as $key => $value) {
                if (isset($_POST['choice' . $i])) {
                    $selected_choice[$key] = $_POST['choice' . $i];
                } else {
                    $selected_choice[$key] = '';
                }
                echo $selected_choice[$key];
                $i++;
                if ($selected_choice[$key] == $correct_choice[$key]) {
                    $score++;
                }
                if ($i == 5) {
                    break;
                }

            }
          
            setcookie('score', $score, time() + 360, "/");
            header('Location: ./result.php');
        }
    }

?>



<!-- ------------------------------------------------------------------------------------------- -->
    <div>
        <form action="" method="post">
            <h2>???? Ai là trợ giảng ???? </h2>
            
            <p><em>Chọn 1 trong 4 câu trả lời A, B, C, D</em></p>
            <br>
            <?php 
            
            echo "<h3>Question</h3>";
            $i = 1;
            foreach ($question as $key => $value) {
                echo "<p>" . $value['question'] . "</p>";
                $listChoice = array('A', 'B', 'C', 'D');
                foreach ($listChoice as $choice) {
                    echo '<div>';
                    $question[$key]['answer'][$choice];
                    echo '<input  type="radio" name="choice'.$i.'" value="' . $choice . '">';
                    echo '<label  for="choice">' . $choice .'. '. '</label>';
                    echo $question[$key]['answer'][$choice];
                    echo '</div>';

                }
                $i++;
            }
            
            
            ?>
            
            <div class="btn">
                <button class="btn_submit"  type="submit" name="submit">Nộp bài</button>
            </div>
            
        </form>

    </div>
</body>
</html>