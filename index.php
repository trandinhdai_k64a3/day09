
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <!-- <link rel="stylesheet" href="./fontawesome-free-6.2.0-web/css/all.css">
    <link rel="stylesheet" href="./fontawesome-free-6.2.0-web/webfonts/fa-brands-400.ttf"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/webfonts/fa-brands-400.ttf" />
    <title>Multiple-choice</title>
</head>
<body>

<?php
    $question = array(
        array(
            'question' => '1. Where is the capital of Vietnam?',
            'answer' => array(
                'A' => 'Tokyo',
                'B' => 'Bangkok',
                'C' => 'Ha Noi',
                'D' => 'Beijing'
            ),
            'correct' => 'C'
        ),
        array(
            'question' => '2. The greatest mountain in the world is?',
            'answer' => array(
                'A' => 'Mount Everest',
                'B' => 'Mount Fuji',
                'C' => 'Mount Kilimanjaro',
                'D' => 'Mount Vinson'
            ),
            'correct' => 'A'
        ),
        array(
            'question' => '3. Where is the country have the most population?',
            'answer' => array(
                'A' => 'America',
                'B' => 'India',
                'C' => 'China',
                'D' => 'Russia'
            ),
            'correct' => 'C'
        ),
        array(
            'question' => '4. Where is the deepest lake in the world?',
            'answer' => array(
                'A' => 'Lake Baikal',
                'B' => 'Lake Tanganyika',
                'C' => 'Lake Superior',
                'D' => 'Lake Victoria'
            ),
            'correct' => 'A'
        ),
        array(
            'question' => '5. War and Peace is a novel by?',
            'answer' => array(
                'A' => 'Pushkin',
                'B' => 'William Shakespeare',
                'C' => 'Leo Tolstoy',
                'D' => 'Charles Dickens'
            ),
            'correct' => 'C'
        )
    );
    
    $correct_choice = array();
    foreach ($question as $key => $value) {
        $correct_choice[$key] = $value['correct'];
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['submit'])) {
            echo 'submit';
            $score = 0;
            $i = 1;
            $selected_choice = array();
            foreach ($question as $key => $value) {
                if (isset($_POST['choice' . $i])) {
                    $selected_choice[$key] = $_POST['choice' . $i];
                } else {
                    $selected_choice[$key] = '';
                }
                echo $selected_choice[$key];
                $i++;
                if ($selected_choice[$key] == $correct_choice[$key]) {
                    $score++;
                }
                if ($i == 6) {
                    break;
                }

            }
            setcookie('score', $score, time() + 400, "/");
            header('Location: ./page2.php');
        }
    }
?>



<!-- ------------------------------------------------------------------------------------------- -->
    <div>
        <form action="" method="post">
            <h2>???? Ai là trợ giảng ???? </h2>
            
            <p><em>Chọn 1 trong 4 câu trả lời A, B, C, D</em></p>
            <br>
            
            
            <?php 
            
            echo "<h3>Question</h3>";
            $i = 1;
            foreach ($question as $key => $value) {
                echo "<p>" . $value['question'] . "</p>";
                $listChoice = array('A', 'B', 'C', 'D');
                foreach ($listChoice as $choice) {
                    echo '<div>';
                    $question[$key]['answer'][$choice];
                    echo '<input  type="radio" name="choice'.$i.'" value="' . $choice . '">';
                    echo '<label  for="choice">' . $choice .'. '. '</label>';
                    echo $question[$key]['answer'][$choice];
                    echo '</div>';

                }
                $i++;
            }
            
            
            ?>
            
            <div class="btn">
                <form></form>
                
                    <button  class="btn_submit"  type="submit" name="submit">Next</button>
                
            </div>
            
        </form>

    </div>
</body>
</html>